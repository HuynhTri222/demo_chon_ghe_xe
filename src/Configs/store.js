import { combineReducers, createStore } from "redux";
import ghe from "../Redux/Reducers/ghe";

const reducer = combineReducers({
    ghe,
});

const store = createStore(reducer);

export default store;