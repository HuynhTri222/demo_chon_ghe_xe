const style = (theme) => {
    return ({
        title: {
            color: theme.palette.title.dark,
        }
    });
}

export default style;