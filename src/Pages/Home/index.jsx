import { Box, Container, Grid, Typography, withStyles } from '@material-ui/core'
import React, { Component } from 'react'
import DanhSachGhe from '../../Components/DanhSachGhe'
import DanhSachGheDangDat from '../../Components/DanhSachGheDangDat'
import style from './style'

class Home extends Component {
    render() {
        return (
            <div>
                <Container maxWidth="md">
                    <Box my={3}>
                        <Typography variant="h3" component="h1" className={this.props.classes.title}>
                            ĐẶT VÉ XE BUS HÃNG CYBERSOFT
                        </Typography>
                    </Box>
                    <Grid container spacing={3}>
                        <Grid item xs={6}>
                            <DanhSachGhe />
                        </Grid>
                        <Grid item xs={6}>
                            <DanhSachGheDangDat />
                        </Grid>
                    </Grid>
                </Container>
            </div>
        )
    }
}

export default withStyles(style, {withTheme: true})(Home);
