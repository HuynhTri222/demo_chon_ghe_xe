import { Box, Grid, withStyles } from '@material-ui/core'
import React, { Component } from 'react'
import GheItem from '../GheItem';
import {connect} from "react-redux";
import { style } from './style';

class DanhSachGhe extends Component {
    render() {
        return (
            <div>
                <Box my={3} borderRadius={5} className={this.props.classes.title} display="flex" justifyContent="center" alignItems="center">
                    <h2>Tài Xế</h2>
                </Box>
                <Grid container spacing={3}>
                    {
                        this.props.danhSachGhe.map((gheItem, index) => {
                            return (<Grid key={index} item xs={3}>
                                        <GheItem gheItem={gheItem}/>
                                    </Grid>);
                    })}
                    
                </Grid>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        danhSachGhe: state.ghe.DanhSachGhe,
        danhSachGheDaChon: state.ghe.DanhSachGheDaChon,
    }
}

export default connect(mapStateToProps)(withStyles(style, {withTheme: true})(DanhSachGhe));
