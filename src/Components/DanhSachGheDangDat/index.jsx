import { Box, Typography, withStyles } from '@material-ui/core'
import React, { Component } from 'react'
import { style } from './style';
import { connect } from "react-redux";

class DanhSachGheDangDat extends Component {
    render() {
        return (
            <div>
                <Box my={3} className={this.props.classes.title} display="flex">
                    <h2>Danh sách ghế đã đặt (5)</h2>
                </Box>
                <Box display="flex" justifyContent="center" alignItems="center">
                    <Typography variant="h5" component="h5">
                        {
                            this.props.danhSachGheDaChon && this.props.danhSachGheDaChon.map((gheItem, index) => {
                                return <p key={index}>Ghế: {gheItem.TenGhe} ${gheItem.Gia} <a href="a" className={this.props.classes.linkHuy}>[Hủy]</a></p>

                            })
                        }
                    </Typography>
                </Box>
                
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        danhSachGheDaChon: state.ghe.DanhSachGheDaChon,
    }
}

export default connect(mapStateToProps)(withStyles(style, {withTheme: true})(DanhSachGheDangDat));


