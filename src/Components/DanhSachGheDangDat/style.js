export const style = (theme) => {
    return ({
        title: {
            justifyContent: 'center',
            alignContent: 'center',
            color: theme.palette.title.dark,
        },
        linkHuy: {
            color: theme.palette.picked.main,
            textDecoration: 'none',
        }
    });
}