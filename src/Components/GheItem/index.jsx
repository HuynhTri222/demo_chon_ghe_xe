import { Button, withStyles } from '@material-ui/core'
import React, { Component } from 'react'
import style from './style';
import { connect } from "react-redux";
import { createAction } from '../../Redux/Actions';
import { CHON_GHE } from '../../Redux/Actions/type';

class GheItem extends Component {

    state = {
        isBooking: false,
    }

    handleOnClick = () => {
        this.props.dispatch(createAction(CHON_GHE, this.props.gheItem));
    }

    handleOnBlur = () => {
        const index = this.props.danhSachGheDaChon.findIndex((item) => item.SoGhe === this.props.gheItem.SoGhe);
        if(index !== -1){
            this.setState({
                isBooking: true,
            })
        } else {
            this.setState({
                isBooking: false,
            })
        }
    }

    render() {
        return (
            <div>
               <Button onClick={this.handleOnClick} onBlur={this.handleOnBlur}  className={ this.props.gheItem.TrangThai ? this.props.classes.btn_Booked :  (this.state.isBooking ? this.props.classes.btn_Selected : this.props.classes.btn_NotSelectedYet)} disabled={this.props.gheItem.TrangThai}>{this.props.gheItem.SoGhe}</Button>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        danhSachGheDaChon: state.ghe.DanhSachGheDaChon,
    }
}

export default connect(mapStateToProps)(withStyles(style, {withTheme: true})(GheItem));
