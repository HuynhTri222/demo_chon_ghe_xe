const style = (theme) => {
    return ({
        btn_NotSelectedYet: {
            width: 50,
            height: 50,
            borderRadius: '0.5',
            backgroundColor: theme.palette.normal.dark,
            color: theme.palette.common.white,
            "&:hover" : {
                backgroundColor: theme.palette.normal.light,
                color: theme.palette.common.black,
            },
            "&:focus" : {
                backgroundColor: theme.palette.picking.dark,
                color: theme.palette.common.black,
                boxShadow: '0 0 0 3px rgba(91, 238, 117, 0.5)'
            },
            
        },
        btn_Selected: {
            width: 50,
            height: 50,
            borderRadius: '0.5',
            backgroundColor: theme.palette.picking.dark,
            color: theme.palette.common.white,
            "&:hover" : {
                backgroundColor: theme.palette.picking.light,
                color: theme.palette.common.black,
            },
            "&:focus" : {
                backgroundColor: theme.palette.picking.dark,
                color: theme.palette.common.black,
                boxShadow: '0 0 0 3px rgba(91, 238, 117, 0.5)'
            },
            
            
        },
        btn_Booked: {
            width: 50,
            height: 50,
            borderRadius: '0.5',
            backgroundColor: theme.palette.picked.main,
            color: theme.palette.common.white,
            },

    });
}

export default style;