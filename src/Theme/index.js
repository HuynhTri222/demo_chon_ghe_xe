import { createMuiTheme } from "@material-ui/core";

const theme = createMuiTheme({
    palette: {
        title: {
            dark: "#D0B933",
        },
        normal: {
            light: "#9aa2ad",
            dark: "#414852",
        },
        picking: {
            light: "#5bee75",
            dark: "#00ba46",
        },
        picked: {
            main: "#EC6D7F",
        },
        background: {
            main: "#9e9e9e"
        }
    },
    height: 50,
});

export default theme;